import { Http } from '@angular/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export class HttpTranslationsLoader {

  constructor(private http: Http) {}

  static HttpLoaderFactory(http: Http) {
      return new TranslateHttpLoader(http, '/assets/translations/i18n-', '.json');
  }
}
