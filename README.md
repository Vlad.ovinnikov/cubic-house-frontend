# CubicHouseFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.1.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).



"@angular/cli": "^1.0.3",
"@angular/common": "^2.4.0",
"@angular/compiler": "^2.4.0",
"@angular/compiler-cli": "^2.4.0",
"@angular/core": "^2.4.0",
"@angular/forms": "^2.4.0",
"@angular/http": "^2.4.0",
"@angular/platform-browser": "^2.4.0",
"@angular/platform-browser-dynamic": "^2.4.0",
"@angular/router": "^3.4.0",
"@ngx-translate/core": "^6.0.1",
"@ngx-translate/http-loader": "0.0.3",
"@types/underscore": "^1.8.0",
"bootstrap": "^4.0.0-alpha.6",
"core-js": "^2.4.1",
"express": "^4.15.3",
"font-awesome": "^4.7.0",
"moment": "^2.18.1",
"ng2-page-scroll": "^4.0.0-beta.7",
"path": "^0.12.7",
"rxjs": "^5.1.0",
"underscore": "^1.8.3",
"zone.js": "^0.7.6"
