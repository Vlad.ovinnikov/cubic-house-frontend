import { CubicHouseFrontendPage } from './app.po';

describe('cubic-house-frontend App', () => {
  let page: CubicHouseFrontendPage;

  beforeEach(() => {
    page = new CubicHouseFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('ch works!');
  });
});
