export const environment = {
  production: true,
  API_URL: 'https://cubic-house-back.herokuapp.com/api'
};
