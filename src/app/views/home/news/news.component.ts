import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { EntryService, AuthService, SpinnerService } from './../../../services';
import { Entry } from './../../../models';

@Component({
  selector: 'ch-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  entries: Entry[];
  perPage: Number;
  page: any;
  pages: Number[];

  constructor(private activatedRoute: ActivatedRoute, private entryService: EntryService, private authService: AuthService, private spinnerService: SpinnerService) {
    this.perPage = 10;
    this.page = 1;
    this.pages = [];
  }

  ngOnInit() {
    this.entryService.entryEvent.emit(null);
    this.spinnerService.runSpinner.emit(true);
    this.getEntries();
  }

  onPageChanged(event) {
    this.spinnerService.runSpinner.emit(true);
    this.page = event;
    this.getEntries();
  }

  getEntries() {

    this.entries = null;
    this.entryService.getList(this.page - 1, this.perPage)
      .subscribe(
        (data: any) => {

          this.entries = data.entries;
          this.page = data.page;
          this.pages = [];
          for (let i = 0; i < Math.ceil(data.pages); i++) {
            this.pages.push(i + 1);
          }
          this.spinnerService.runSpinner.emit(false);
        },
        err => {
          this.entryService.entryEvent.emit({ message: err.message, error: true });
          this.spinnerService.runSpinner.emit(false);
        }
      );
  }

}
