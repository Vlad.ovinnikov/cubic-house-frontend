import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Entry, Category } from './../../../models';
import { EntryService, CategoryService } from './../../../services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ch-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {

  entry: Entry;
  categories: Category[];
  entryId: string;

  constructor(private entryService: EntryService, private activatedRoute: ActivatedRoute, private router: Router, private translate: TranslateService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.entryId = this.activatedRoute.snapshot.params['id'];
    // get entry by its id
    this.entryService.get(this.entryId)
      .subscribe(
        (data: Entry) => {
          this.entry = data;
        },
        (err: any) => this.entryService.entryEvent.emit({ message: err.message, error: true })
      );
    // get all category
    this.categoryService.getList()
      .subscribe(
        (data: Category[]) => {
          this.categories = data;
        },
        (err: any) => this.entryService.entryEvent.emit({ message: err.message, error: true })
      );
  }

  submitData(event) {
    event._id = this.entry._id;
    event.author = this.entry.author;
    this.entryService.update(event)
      .subscribe(
        (data: Entry) => {
          this.entry = data;
          this.entryService.entryEvent.emit({ message: this.translate.instant('EDIT_POST.SUCCESS_MESSAGE'), error: false });
          this.router.navigate(['/posts', data._id]);
        },
        (err: any) => this.entryService.entryEvent.emit({ message: err.message, error: true })
      );
  }

 }
