import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'underscore';

import { Entry, Category } from './../../../models';
import { EntryService, CategoryService } from './../../../services';

@Component({
  selector: 'ch-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {

  entry: Entry;
  categoryId: string;
  categories: Category[];
  categorySelected: Category;

  constructor(private entryService: EntryService, private categoryService: CategoryService, private activatedRoute: ActivatedRoute, private router: Router) {
      this.entry = new Entry();
      this.entry.priority = 0;
      this.entry.status = 0;
    }

  ngOnInit() {
    // get category id
    this.categoryId = this.activatedRoute.snapshot.params['id'];
    // get categories
    this.categoryService.getList()
      .subscribe(
        (data: Category[]) => {
          this.categories = data;
          this.categorySelected = _.findWhere(data, {_id: this.categoryId});
        },
        (err: any) => this.entryService.entryEvent.emit({ message: err.message, error: true })
      );
  }

  submitData(entry: Entry) {
    entry.category = new Category();
    entry.category._id = this.categoryId;

    this.entryService.add(entry)
      .subscribe(
        (data: any) => {
          this.router.navigate(['/posts', data.entry._id]);
          this.entryService.entryEvent.emit({ message: data.message, error: false });
        },
        err => this.entryService.entryEvent.emit({ message: err.message, error: true })
      );
  }

}
