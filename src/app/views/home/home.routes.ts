import { Routes } from '@angular/router';

import { AuthGuardService } from './../../services';
import { NewsComponent, PostsComponent, PostDetailsComponent, AddPostComponent, EditPostComponent } from './';

export const HOME_ROUTES: Routes = [

  { path: '', redirectTo: 'news', pathMatch: 'full', canActivate: [AuthGuardService] },
  {
    path: 'news',
    component: NewsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'categories/:id/posts',
    component: PostsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'categories/:id/posts/add',
    component: AddPostComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'posts/:id',
    component: PostDetailsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'posts/:id/edit',
    component: EditPostComponent,
    canActivate: [AuthGuardService]
  }
];
