import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { CategoryService, AuthService, EntryService, CommentService, SpinnerService } from './../../services';
import { Category, Entry, Comment } from './../../models';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  categories: Category[];
  recentEntries: Entry[];
  recentComments: Comment[];
  message: any;
  entryEventSub: any;
  commentEventSub: any;
  isRequesting: boolean;
  subSpinner: any;
  showMenu: boolean;

  constructor(private categoryService: CategoryService, private authService: AuthService, private entryService: EntryService, private router: Router, private commentService: CommentService, private spinnerService: SpinnerService) { }

  ngOnInit() {
    this.isRequesting = true;
    this.message = null;
    this.showMenu = false;

    this.subSpinner = this.spinnerService.runSpinner
      .subscribe(
        (data: boolean) => {
          this.isRequesting = data;
        }
      );

      // get all categories
      this.getCategories();
      // get recent entries
      this.getRecentEntries();
      // get recent comments
      this.getRecentComments();

    // message comes from children components
    this.entryEventSub = this.entryService.entryEvent
      .subscribe(
        (message: any) => {
          this.isRequesting = true;
          if(message) {
            if (!message.error) {
              this.message = message;
              this.getRecentEntries();
              setTimeout(() => {
                this.message = null;
                this.isRequesting = false;
              }, 5000);
            } else {
              this.message = message;
            }
          } else {
            this.message = null;
          }
        }
      );

    // message comes from children components
    this.commentEventSub = this.commentService.commentEvent
      .subscribe(
        (message: any) => {
          if (!message.error) {
            this.getRecentComments();
          } else {
            this.message = message;
          }
        }
      );
  }

  openMenu() {
    console.log('click');
    this.showMenu = !this.showMenu;
  }

  private getCategories(): void {
    /**
     * get category list
     * @return {[type]} [description]
     */
    this.categoryService.getList()
      .subscribe(
        (data: Category[]) => {

          this.categoryService.categoriesSharing.emit(data);
          this.categories = data;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: 'danger' };
        }
      );
  }

  private getRecentComments(): void {

    this.commentService.getRecent()
      .subscribe(
        (comments: Comment[]) => {
          this.recentComments = comments;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: 'danger' };
        }
      );
  }

  private getRecentEntries(): void {
    /**
     * get 5 recent entries
     * @return {[type]} [description]
     */
    this.entryService.getRecent()
      .subscribe(
        (data: Entry[]) => {
          this.recentEntries = data;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: 'danger' };
        }
      );
  }

  ngOnDestroy() {
    this.entryEventSub.unsubscribe();
    this.commentEventSub.unsubscribe();
    this.subSpinner.unsubscribe();
  }
}
