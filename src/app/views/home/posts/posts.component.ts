import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { EntryService, CategoryService, SpinnerService } from './../../../services';
import { Category, Entry } from './../../../models';

@Component({
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, OnDestroy {

  entries: Entry[];
  perPage: number;
  page: number;
  pages: number[];
  categoryId: string;
  subSelectCategory: any;

  constructor(private activatedRoute: ActivatedRoute, private entryService: EntryService, private categoryService: CategoryService, private spinnerService: SpinnerService, private translate: TranslateService) {
    this.perPage = 10;
    this.page = 1;
    this.pages = [];
  }

  ngOnInit() {
    // get category id from url
    this.categoryId = this.activatedRoute.snapshot.params['id'];
    // get entries with category id
    this.getEntries(this.categoryId);
    // subscribe on select category event
    this.subSelectCategory = this.categoryService.selectCategory
      .subscribe(
        (data: Category) => {
          this.entryService.entryEvent.emit(null);
          this.spinnerService.runSpinner.emit(true);
          this.getEntries(data._id);
        }
      );
  }

  onPageChanged(itemEvent: any) {
    this.spinnerService.runSpinner.emit(true);
    this.page = itemEvent;
    this.getEntries(this.categoryId);
  }

  getEntries(id: string) {
    this.entries = null;
    this.entryService.getListWithCategory(id, this.page - 1, this.perPage)
      .subscribe(
        (data: any) => {
          this.entries = data.entries;
          this.page = data.page;
          this.pages = [];
          for (let i = 0; i < Math.ceil(data.pages); i++) {
            this.pages.push(i + 1);
          }
          this.spinnerService.runSpinner.emit(false);
        },
        (err: any) => {
          console.log(err);
          if(err.message) {
            this.entryService.entryEvent.emit({ message: err.message, error: 'warning' });
          } else {
            this.entryService.entryEvent.emit({ message: this.translate.instant('ERROR.MESSAGE'), error: 'danger' });
          }

          this.spinnerService.runSpinner.emit(false);
        }
      );
  }

  ngOnDestroy() {
    this.subSelectCategory.unsubscribe();
  }
}
