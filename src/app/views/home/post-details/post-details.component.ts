import { Component, OnInit, AfterViewChecked, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageScrollService, PageScrollConfig} from 'ng2-page-scroll';

import { EntryService, LocalStorageService, CommentService, SpinnerService } from './../../../services';
import { Entry, Comment, Profile } from './../../../models';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ch-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit, AfterViewChecked, OnDestroy {

  entry: Entry;
  comments: Comment[];
  idParam: string;
  routeFragmentSubscription: any;
  profile: Profile;
  entrySelectEventSub: any;
  commentMessage: any;
  mainItem: Comment;

  constructor(private entryService: EntryService, private activatedRoute: ActivatedRoute, private localStorageService: LocalStorageService, private commentService: CommentService, private router: Router, private spinnerService: SpinnerService, private translate: TranslateService) {
    this.mainItem = new Comment();
    this.profile = this.localStorageService.getObject('ch.profile');

    // PageScrollConfig.defaultScrollOffset = 150;
    // PageScrollConfig.defaultEasingLogic = {
    //   ease: (t: number, b: number, c: number, d: number): number => {
    //     // easeInOutExpo easing
    //     if(t === 0) return b;
    //     if(t === d) return b + c;
    //     if((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
    //     return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    //   }
    // };
  }

  ngOnInit() {
    this.spinnerService.runSpinner.emit(true);
    // get entry id from url
    this.idParam = this.activatedRoute.snapshot.params['id'];
    // subscribe on entry select event
    this.entrySelectEventSub = this.entryService.selectEntry
      .subscribe(
        (entry: Entry) => {
          this.entry = entry;
          this.getComments(entry._id);
        }
    );
    this.getEntry(this.idParam);

    this.commentService.commentEvent
      .subscribe(
        (data: any) => {
          this.spinnerService.runSpinner.emit(true);
          this.getEntry(this.idParam);
        }
      );
  }

  getEntry(id): void {
    // get entry with its id
    this.entryService.get(id)
      .subscribe(
        (entry: Entry) => {
          this.entry = entry;
          this.getComments(id);
        },
        err => this.entryService.entryEvent.emit({ message: err.message, error: true })
      );
  }

  getCommentsOnEvent(): void {
    this.getComments(this.entry._id);
  }

  getComments(id: string): void {
    this.commentService.getList(id)
      .subscribe(
        (comments: any[]) => {
          this.comments = comments;
          this.spinnerService.runSpinner.emit(false);
        },
        (err: any) => console.error(err)
      );
  }

  onAddComment(comment: Comment): void {
    comment.entry = this.idParam;
    this.commentService.add(comment)
      .subscribe(
        (data: Comment) => {
          this.commentMessage = { message: this.translate.instant('COMMENT.SUCCESS_ADD'), error: false };
          setTimeout(() => {
            this.commentService.commentEvent.emit({ mmessage: '', error: false });
            this.getComments(this.idParam);
            this.commentMessage = null;
            this.getEntry(this.idParam);
          }, 3000);
        },
        (err) => this.commentMessage = { message: err.message, error: true }
      );
  }

  ngAfterViewChecked(): void {
    this.routeFragmentSubscription = this.activatedRoute.fragment
      .subscribe(fragment => {
        if (fragment) {
          const element = document.getElementById(fragment);
          if (element) {
            element.scrollIntoView();
          }
        }
      });
  }

  removeEntry(entry: Entry): void {
    this.entryService.remove(entry._id)
      .subscribe(
        (data: any) => {
          this.router.navigate([`/categories/${ entry.category._id }/posts`]);
        },
        (err: any) => this.entryService.entryEvent.emit({ message: err.message, error: true })
      );
  }

  ngOnDestroy() {
    this.routeFragmentSubscription.unsubscribe();
    this.entrySelectEventSub.unsubscribe();
  }

}
