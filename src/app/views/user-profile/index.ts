export { UserProfileComponent } from './user-profile.component';
export { ProfileComponent } from './profile/profile.component';
export { PasswordComponent } from './password/password.component';
export { EmailComponent } from './email/email.component';
