import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { User } from './../../../models';
import { UserService, LocalStorageService } from './../../../services';

@Component({
  selector: 'ch-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

  @Output() message: EventEmitter<any>;
  @Output() isRequesting: EventEmitter<boolean>;
  @Input() user: User;
  isEdit: boolean;

  // password controls
  passwordForm: FormGroup;
  password: FormControl;

  constructor(private translate: TranslateService, private userService: UserService, private localStorageService: LocalStorageService) {
    this.isRequesting = new EventEmitter<boolean>();
    this.message = new EventEmitter<any>();
    this.isEdit = false;
  }

  ngOnInit() {
    // create password controls and from
    this.createControls();
    this.createForm();
  }

  update() {
    this.isRequesting.emit(true);
    this.isEdit = !this.isEdit;
    this.userService.updatePassword(this.passwordForm.value.password)
      .subscribe(
        (data: any) => {
          this.localStorageService.set('ch.user', data.user);
          this.user = data.user;
          this.message.emit({ message: data.message, error: false });
          this.isRequesting.emit(false);
          setTimeout(() => { this.message.emit(null) }, 5000);
        },
        (err: any) => {
          this.message.emit({ message: this.translate.instant('PROFILE.ERROR_UPDATE'), error: true });
          this.isRequesting.emit(false);
        }
      );
  }

  private createControls() {
    this.password = new FormControl(null, Validators.compose([
      Validators.required,
      Validators.pattern(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/)
    ]))
  }

  private createForm() {
    this.passwordForm = new FormGroup({
      'password': this.password
    })
  }

}
