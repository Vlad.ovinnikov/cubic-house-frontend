import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { User } from './../../../models';
import { UserService, LocalStorageService } from './../../../services';

@Component({
  selector: 'ch-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {

  @Output() message: EventEmitter<any>;
  @Output() isRequesting: EventEmitter<boolean>;
  @Input() user: User;
  isEdit: boolean;

  // email form controls
  emailForm: FormGroup;
  email: FormControl;

  constructor(private userService: UserService, private translate: TranslateService, private localStorageService: LocalStorageService) {
    this.isRequesting = new EventEmitter<boolean>();
    this.message = new EventEmitter<any>();
    this.isEdit = false;
  }

  ngOnInit() {
    // create email controls and from
    this.createControls();
    this.createForm();
  }

  edit() {
    this.isEdit = !this.isEdit;
    this.emailForm.setValue({
      'email': this.user.email
    });
  }

  update() {
    this.isRequesting.emit(true);
    this.isEdit = !this.isEdit;
    this.userService.updateEmail(this.emailForm.value.email)
      .subscribe(
        (data: any) => {
          this.localStorageService.set('ch.user', data.user);
          this.user = data.user;
          this.message.emit({ message: data.message, error: false });
          this.isRequesting.emit(false);
          setTimeout(() => { this.message.emit(null) }, 5000);
        },
        (err: any) => {
          this.message.emit({ message: this.translate.instant('PROFILE.ERROR_UPDATE'), error: true });
          this.isRequesting.emit(false);
        }
      );
  }

  private createControls() {
    this.email = new FormControl(null, Validators.compose([
      Validators.required,
      this.isEmail
    ]));
  }

  private createForm() {
    this.emailForm = new FormGroup({
      'email': this.email
    })
  }

  private isEmail(control: FormControl): {[s: string]: boolean} {
    if (control.value) {
      if (!control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
        return { noEmail: true };
      }
    }

  }

}
