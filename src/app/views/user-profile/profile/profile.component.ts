import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as _ from 'underscore';
import * as  moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

import { UserService, LocalStorageService } from './../../../services';
import { Profile } from './../../../models';

@Component({
  selector: 'ch-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @Output() message: EventEmitter<any>;
  @Output() isRequesting: EventEmitter<boolean>;
  @Input() profile: Profile;
  isEdit: boolean;

  // profile form controls
  profileForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  birthdate: FormGroup;
  day: FormControl;
  month: FormControl;
  year: FormControl;
  gender: FormControl;

  constructor(private userService: UserService, private translate: TranslateService, private localStorageService: LocalStorageService) {
    this.isRequesting = new EventEmitter<boolean>();
    this.message = new EventEmitter<any>();
    this.isEdit = false;
  }

  ngOnInit() {
    // create profile controls and form
    this.createControls();
    this.createForm()
  }

  update() {
    this.isRequesting.emit(true);
    this.isEdit = !this.isEdit;
    let profileUpdated = _.pick(this.profileForm.value, ['firstName', 'lastName', 'gender']);
    profileUpdated.birthdate = `${ this.profileForm.value.birthdate.year }-${ this.profileForm.value.birthdate.month }-${ this.profileForm.value.birthdate.day }`;

    this.userService.updateProfile(profileUpdated)
      .subscribe(
        (data: any) => {
          this.localStorageService.set('ch.profile', data.profile);
          this.profile = data.profile;
          this.message.emit({ message: data.message, error: 'success' });
          this.isRequesting.emit(false);
          setTimeout(() => { this.message.emit(null) }, 3000);
        },
        (err: any) => {
          this.message.emit({ message: this.translate.instant('PROFILE.ERROR_UPDATE'), error: 'danger' });
          this.isRequesting.emit(false);
        }
      )
  }

  edit() {
    this.isEdit = !this.isEdit;
    this.profileForm.setValue({
      'firstName': this.profile.firstName,
      'lastName': this.profile.lastName,
      'birthdate': {
        'day': this.profile.birthdate ? moment(this.profile.birthdate).date(): '01',
        'month': this.profile.birthdate ? moment(this.profile.birthdate).format('MM') : '01',
        'year': this.profile.birthdate ? moment(this.profile.birthdate).year() : '1970'
      },
      'gender': this.profile.gender
    });

  }

  private createControls() {
    this.firstName = new FormControl(null);
    this.lastName = new FormControl(null);

    this.day = new FormControl(null);
    this.month = new FormControl(null);
    this.year = new FormControl(null);

    this.birthdate = new FormGroup({
      'day': this.day,
      'month': this.month,
      'year': this.year
    });
    this.gender = new FormControl(null);
  }

  private createForm() {
    this.profileForm = new FormGroup({
      'firstName': this.firstName,
      'lastName': this.lastName,
      'birthdate': this.birthdate,
      'gender': this.gender
    });

  }
}
