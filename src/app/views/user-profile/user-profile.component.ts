import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { Profile, User } from './../../models';
import { UserService } from './../../services';

@Component({
  selector: 'ch-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  message: any;
  profile: Profile;
  user: User;
  isRequesting: boolean;

  constructor(private userService: UserService) {
    this.message = null;
    this.profile = new Profile();
    this.user = new User();
    this.isRequesting = true;
  }

  ngOnInit() {
    // get profile info
    this.userService.getProfile()
      .subscribe(
        (data: any) => {
          this.profile = data.profile;
          let profile = _.pick(data.profile, ['firstName', 'lastName', 'birthdate', 'gender']);
          this.user = data.user;
          this.isRequesting = false;
        }
      );
  }

  request(event: boolean) {
    this.isRequesting = event;
  }

  getMessage(event: any) {
    this.message = event;
  }

}
