import { Component, OnInit } from '@angular/core';

import { EntryService, CategoryService, CommentService } from './../../services';
import { Entry, Category, Comment } from './../../models';

@Component({
  selector: 'ch-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.scss']
})
export class UserPostsComponent implements OnInit {

  message: any = null;
  perPage: number;
  page: number;
  pages: number[];
  entries: Entry[];
  categories: Category[];
  currentCategory: Category;
  recentEntries: Entry[];
  recentComments: Comment[];
  isRequesting: boolean;

  constructor(private entryService: EntryService, private categoryService: CategoryService, private commentService: CommentService) {
    this.message = null;
    this.perPage = 10;
    this.page = 1;
    this.currentCategory = new Category();
    this.isRequesting = true;
    this.currentCategory._id = null;
    this.currentCategory.title = 'USER_POSTS.ALL';
  }

  ngOnInit() {
      this.getCategories();
      this.getEntries(this.currentCategory);
      this.getRecentComments();
      this.getRecentEntries();
  }

  getEntries(category: Category) {
    this.currentCategory = category;
    if (!this.isRequesting) {
      this.isRequesting = true;
    }
    this.entries = null;
    this.message = null;
    if (category._id !== null) {
      // get user entries for selected category
      this.entryService.getUserListWithCategory(category._id, this.page - 1, this.perPage)
        .subscribe(
          (data: any) => {
            this.isRequesting = false;
            this.entries = data.entries;
            this.page = data.page;
            this.pages = [];
            for (let i = 0; i < Math.ceil(data.pages); i++) {
              this.pages.push(i + 1);
            }
          },
          (err: any) => {
            this.isRequesting = false;
            this.message = { message: err.message, error: true };
          }
        );
    } else {
      // get all user entries
      this.entryService.getUserList(this.page - 1, this.perPage)
        .subscribe(
          (data: any) => {
            this.isRequesting = false;
            this.entries = data.entries;
            this.page = data.page;
            this.pages = [];
            for (let i = 0; i < Math.ceil(data.pages); i++) {
              this.pages.push(i + 1);
            }
          },
          (err: any) => {
            this.isRequesting = false;
            this.message = { message: err.message, error: true };
          }
        );
    }
  }
  private getCategories() {
    /**
     * get category list
     * @return {[type]} [description]
     */
    this.categoryService.getList()
      .subscribe(
        (data: Category[]) => {
          data.unshift(this.currentCategory);
          this.categories = data;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: true };
        }
      );
  }

  private getRecentEntries() {
    /**
     * get 5 recent entries
     * @return {[type]} [description]
     */
    this.entryService.getRecent()
      .subscribe(
        (data: Entry[]) => {
          this.recentEntries = data;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: true };
        }
      );
  }

  private getRecentComments() {
    /**
     * get 5 recent comments
     * @return {[type]} [description]
     */
    this.commentService.getRecent()
      .subscribe(
        (comments: Comment[]) => {
          this.recentComments = comments;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: true };
        }
      );
  }

}
