import { Component, OnInit, OnDestroy } from '@angular/core';

import { SpinnerService } from './../../services';

@Component({
  templateUrl: './enter.component.html',
  styleUrls: ['./enter.component.scss']
})
export class EnterComponent implements OnInit, OnDestroy {

  subSpinner: any;
  isRequesting: boolean;

  constructor(private spinnerService: SpinnerService) { }

  ngOnInit() {
    // subscribe for spinner events
    this.subSpinner = this.spinnerService.runSpinner
      .subscribe(
        (data: boolean) => {
          this.isRequesting = data;
        }
      );
  }

  ngOnDestroy() {
    this.subSpinner.unsubscribe();
  }
}
