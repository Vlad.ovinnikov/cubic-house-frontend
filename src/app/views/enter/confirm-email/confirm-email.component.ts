import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService, SpinnerService } from './../../../services';

@Component({
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  token: string;
  message: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private authService: AuthService, private spinnerService: SpinnerService) {
    this.token = '';
    this.message = null;
  }

  ngOnInit() {
    // run spinner
    this.spinnerService.runSpinner.emit(true);
    // get token from url
    this.token = this.activatedRoute.snapshot.params['token'];
    this.authService.confirmEmail(this.token)
      .subscribe(
        (data: any) => {
          this.message = { message: data.message, error: 'success'};
          localStorage.setItem('ch.emailForLogin', data.user.email);
          // hide spinner
          this.spinnerService.runSpinner.emit(false);
          setTimeout(() => {
            this.router.navigate(['/enter/login']);
          }, 5000);
        },
        (err: any) => {
          // hide spinner
          this.spinnerService.runSpinner.emit(false);

          this.message = { message: err.message, error: 'danger' };
        }
      );
  }

}
