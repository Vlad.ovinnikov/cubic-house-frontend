import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AuthService, LocalStorageService, SpinnerService } from './../../../services';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  model: any = {};
  message: any;

  constructor(private authService: AuthService, private localStorageService: LocalStorageService, private spinnerService: SpinnerService, private translate: TranslateService) {
  }

  ngOnInit() {
    // run spinner
    this.spinnerService.runSpinner.emit(true);
    // reset login status
    const token = this.localStorageService.get('ch.jwt_token');
    if (token) {
      this.authService.logout()
        .subscribe(
          data => {
            this.authService.statusAuth.emit('logout');
          },
          err => console.error(err)
        );
    }

    this.model.email = this.localStorageService.get('ch.emailForLogin');
  }

  ngAfterViewInit() {
    // hide spinner
    this.spinnerService.runSpinner.emit(false);
  }

  onLogin() {
    this.message = null;
    // run spinner
    this.spinnerService.runSpinner.emit(true);
    // login request
    this.authService.login(this.model)
        .subscribe(
          (data: any) => {
            this.authService.statusAuth.emit('login');
            // remove email from localStorage for autofill field
            this.localStorageService.removeItem('ch.emailForLogin');
            // hide spinner
            this.spinnerService.runSpinner.emit(false);
           },
          err => {
            // hide spinner
            this.spinnerService.runSpinner.emit(false);
            // show error message
            if(err.message) {
              this.message = { message: err.message, error: 'danger' };
            } else {
              this.message = { message: this.translate.instant('ERROR.MESSAGE'), error: 'danger' };
            }
          }
        );
  }

}
