import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { AuthService, SpinnerService } from './../../../services';

@Component({
  selector: 'ch-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  message: any = null;
  token: string = null;
  resetPasswordForm: FormGroup;
  password: FormControl;
  confirmPassword: FormControl;

  constructor(private fb: FormBuilder, private authService: AuthService, private activatedRoute: ActivatedRoute, private spinnerService: SpinnerService) { }

  ngOnInit() {
    this.token = this.activatedRoute.snapshot.params['token'];
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.password = new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/)
    ]));

    this.confirmPassword = new FormControl('', Validators.compose([
      Validators.required,
      this.isEqualPassword.bind(this)
    ]));
  }

  createForm() {
    this.resetPasswordForm = new FormGroup({
      password: this.password,
      confirmPassword: this.confirmPassword
    });
  }

  isEqualPassword(control: FormControl): {[s: string]: boolean} {
    if (!this.resetPasswordForm) {
      return { passwordsNotMatch: true };
    }

    if (control.value !== this.resetPasswordForm.controls['password'].value) {
      return { passwordsNotMatch: true };
    }
  }

  onReset() {
    // run spinner
    this.spinnerService.runSpinner.emit(true);
    this.message = null;
    this.authService.resetPassword(this.token, this.password.value)
      .subscribe(
        data => {
          // hide spinner
          this.spinnerService.runSpinner.emit(false);
          this.message = { message: data, error: 'success' };
        },
        err => {
          // hide spinner
          this.spinnerService.runSpinner.emit(false);
          this.message = { message: err.message, error: 'danger' };
        }
      );
  }
}
