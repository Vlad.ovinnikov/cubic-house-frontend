import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService, SpinnerService } from './../../../services';

@Component({
  selector: 'ch-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, AfterViewInit {

  signupForm: FormGroup;
  email: FormControl;
  password: FormControl;
  confirmPassword: FormControl;
  gender: FormControl;
  message: any;

  constructor(private authService: AuthService, private spinnerService: SpinnerService) {
    this.message = null;
  }

  ngOnInit(): any {
    // run spinner
    this.spinnerService.runSpinner.emit(true);
    // init form controls
    this.createFormControls();
    // init form
    this.createForm();
  }

  ngAfterViewInit() {
    // hide spinner
    this.spinnerService.runSpinner.emit(false);
  }

  private createFormControls() {
    this.email = new FormControl('', Validators.compose([
      Validators.required,
      this.isEmail
    ]));

    this.password = new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/)
    ]));

    this.confirmPassword = new FormControl('', Validators.compose([
      Validators.required,
      this.isEqualPassword.bind(this)
    ]));
    this.gender = new FormControl(true);
  }

  private createForm() {
    this.signupForm = new FormGroup({
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword,
      gender: this.gender
    });
  }

  onSignup() {
    this.message = null;
    // run spinner
    this.spinnerService.runSpinner.emit(true);

    this.authService.signup(this.signupForm.value)
        .subscribe(
          (data: any) => {
            // hide spinner
            this.spinnerService.runSpinner.emit(false);
            this.message = { message: data.message, error: 'success' }
          },
          err => {
            // hide spinner
            this.spinnerService.runSpinner.emit(false);
            this.message = { message: err.message, error: 'danger' }
          }
        );
  }

  private isEmail(control: FormControl): {[s: string]: boolean} {
    if (!control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
      return { noEmail: true };
    }
  }

  private isEqualPassword(control: FormControl): {[s: string]: boolean} {
    if (!this.signupForm) {
      return { passwordsNotMatch: true };
    }

    if (control.value !== this.signupForm.controls['password'].value) {
      return { passwordsNotMatch: true };
    }

    return null;
  }
}
