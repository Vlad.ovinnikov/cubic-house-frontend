import { Component } from '@angular/core';

import { AuthService, SpinnerService } from './../../../services';

@Component({
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  model: any = {};
  message: any = null;

  constructor(private authService: AuthService, private spinnerService: SpinnerService) { }

  onResetPassword() {
    // run spinner
    this.spinnerService.runSpinner.emit(true);
    this.authService.forgotPassword(this.model)
      .subscribe(
        (data: any) => {
          // hide spinner
          this.spinnerService.runSpinner.emit(false);
          this.message = { message: data.message, error: 'success' };
        },
        (err: any) => {
          // hide spinner
          this.spinnerService.runSpinner.emit(false);
          this.message = { message: err.message, error: 'danger' };
        }
      );
  }
}
