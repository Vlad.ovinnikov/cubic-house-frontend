import { Routes } from '@angular/router';

import {
  LoginComponent,
  ConfirmEmailComponent,
  SignupComponent,
  ForgotPasswordComponent,
  ResetPasswordComponent } from './';

export const ENTER_ROUTES: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'forgot', component: ForgotPasswordComponent},
  { path: 'reset/:token', component: ResetPasswordComponent },
  { path: 'email-confirm/:token', component: ConfirmEmailComponent }

];
