import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as _ from 'underscore';

import { Entry, Category } from './../../../models';

@Component({
  selector: 'ch-add-edit-post',
  templateUrl: './add-edit-post.component.html',
  styleUrls: ['./add-edit-post.component.scss']
})
export class AddEditPostComponent implements OnInit {

  @Input()item: Entry;
  @Input()categorySelected: Category;
  @Input()categories: Category[];
  @Input()isEdit: boolean;
  @Output()onClick: EventEmitter<Entry>;

  entryForm: FormGroup;
  title: FormControl;
  description: FormControl;
  category: FormControl;
  priority: FormControl;
  status: FormControl;

  priorities: string[] = ['ADD_POST.NONE', 'ADD_POST.LOW', 'ADD_POST.HIGH'];
  statuses: string[] = ['ADD_POST.DRAFT', 'ADD_POST.PUBLISHED', 'ADD_POST.ARHIVED'];

  constructor() {
    this.onClick = new EventEmitter<Entry>();
  }

  ngOnInit() {
    this.createControls();
    this.createForm();
    if(this.isEdit) {
      this.entryForm.setValue(_.pick(this.item, ['title', 'description', 'category', 'priority', 'status']));
    }
  }

  private createControls() {
    this.title = new FormControl(null, Validators.compose([
      Validators.required,
      Validators.minLength(3)
    ]));

    this.description = new FormControl(null, Validators.compose([
      Validators.required,
      Validators.minLength(10)
    ]));

    this.category = new FormControl(null);
    this.priority = new FormControl(null);
    this.status = new FormControl(null);
  }

  private createForm() {
    this.entryForm = new FormGroup({
      'title': this.title,
      'description': this.description,
      'category': this.category,
      'priority': this.priority,
      'status': this.status
    });
  }

  onSubmit(event: Event) {
    event.preventDefault();
    let entryEdited = new Entry();
    entryEdited = this.entryForm.value;
    this.onClick.emit(entryEdited);
  }
}
