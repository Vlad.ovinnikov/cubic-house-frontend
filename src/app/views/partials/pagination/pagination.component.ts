import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ch-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Output() itemEvent = new EventEmitter<Number>();
  @Input() items: Number[];
  @Input() currentItem: Number;

  constructor() { }

  ngOnInit() {
  }

  selectItem(item) {
    this.currentItem = item;
    this.itemEvent.emit(item);
  }

}
