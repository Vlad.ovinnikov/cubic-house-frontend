import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Comment, Profile } from './../../../../models/index';

@Component({
  selector: 'ch-comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.scss']
})
export class CommentItemComponent implements OnInit {

  @Input()item: Comment;
  @Input()user: Profile;
  @Input()isEdit: boolean;
  @Output()onEditClickBtn: EventEmitter<Comment>;
  @Output()onRemoveClickBtn: EventEmitter<string>;

  constructor() {
    this.onEditClickBtn = new EventEmitter<Comment>();
    this.onRemoveClickBtn = new EventEmitter<string>();

  }

  ngOnInit() {
  }

  edit() {
    this.onEditClickBtn.emit(this.item);
  }

  remove() {
    this.onRemoveClickBtn.emit(this.item._id);
  }

}
