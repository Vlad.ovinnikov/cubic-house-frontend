import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Comment, Profile } from './../../../models/index';
import { CommentService } from './../../../services/index';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ch-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input()comments;
  @Input()entryId: string;
  @Input()user: Profile;
  @Output()onCommentSave: EventEmitter<string>;

  isEdit: boolean;
  isReply: boolean;
  currentItem: Comment;
  commentToEdit: Comment;
  commentMessage: any;

  constructor(private commentService: CommentService, private translate: TranslateService) {
    this.onCommentSave = new EventEmitter<string>();
    this.commentToEdit = new Comment();
    this.currentItem = new Comment();
    this.isEdit = false;
    this.isReply = false;
  }

  ngOnInit() {
  }

  remove(commentId: string): void {
    this.commentService.remove(this.entryId, commentId)
      .subscribe(
        (data: any) => {
          this.commentMessage = { message: this.translate.instant('COMMENT.SUCCESS_REMOVE'), error: false };
          this.onCommentSave.emit('getComments');
          this.commentService.commentEvent.emit({ message: 'get entry', error: false });

          setTimeout(() => {
            this.commentMessage = null;

          }, 3000);
        },
        (err) => {
          this.commentMessage = { message: err.message, error: true };
          this.commentService.commentEvent.emit({ message: err.message, error: true });
        }
      );
  }

  edit(comment: Comment): void {
    this.isEdit = !this.isEdit;
    this.commentToEdit = comment;
  }

  cancel(event): void {
    // this.currentIndex = 'main';
    this.currentItem = new Comment();
    this.commentToEdit = new Comment();
    this.isEdit = false;
    this.isReply = false;
  }

  onEditSubmit(comment: Comment): void {
    this.commentService.update(comment)
      .subscribe(
        (data: any) => {
          this.commentMessage = { message: this.translate.instant('COMMENT.SUCCESS_UPDATE'), error: false };
          this.onCommentSave.emit('getComments');
          this.commentService.commentEvent.emit({ message: 'get entry', error: false });

          setTimeout(() => {
            this.isEdit = !this.isEdit;
            this.commentToEdit = new Comment();
            this.commentMessage = null;
          }, 3000);
        },
        (err) => this.commentMessage = { message: err.message, error: true }
      );
  }

  onSubmit(comment: Comment, parentId: string): void {
    comment.parentId = parentId;
    comment.entry = this.entryId;

    this.commentService.add(comment)
      .subscribe(
        (data: Comment) => {
          this.onCommentSave.emit('getComments');
          this.commentService.commentEvent.emit({ message: 'get entry', error: false });
          this.commentMessage = { message: this.translate.instant('COMMENT.SUCCESS_ADD'), error: false };

          setTimeout(() => {
            this.commentMessage = null;
          }, 3000);
        },
        (err) => this.commentMessage = { message: err.message, error: true }
      );
  }

}
