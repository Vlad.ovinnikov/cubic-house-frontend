import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { CommentService } from './../../../../services/index';
import { Comment } from './../../../../models/index';

@Component({
  selector: 'ch-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.scss']
})
export class ReplyComponent implements OnInit {

  commentForm: FormGroup;
  text: FormControl;

  @Output()onSubmitClick: EventEmitter<any>;
  @Output()onCancelClick: EventEmitter<any>;
  @Input()showReply: boolean;
  @Input()message: any;
  @Input()ifMain: boolean;
  @Input()currentItem: Comment;
  @Input()item: Comment;
  @Input()titleForm: string;

  constructor(private commentService: CommentService) {
    this.onSubmitClick = new EventEmitter<any>();
    this.onCancelClick = new EventEmitter<any>();
  }

  ngOnInit() {
    // init commetn form
    this.createForm();
  }

  createForm() {
    this.text = new FormControl(this.currentItem.text, Validators.compose([Validators.required, Validators.minLength(3)]));

    this.commentForm = new FormGroup({
      'text': this.text
    });
  }

  onCancel() {
    this.onCancelClick.emit('click');
  }

  onSubmit() {
    this.currentItem.text = this.commentForm.value.text;
    this.onSubmitClick.emit(this.currentItem);
    this.commentForm.reset();
  }

}
