import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentShortComponent } from './comment-short.component';

describe('CommentShortComponent', () => {
  let component: CommentShortComponent;
  let fixture: ComponentFixture<CommentShortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentShortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentShortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
