import { Component, OnInit, Input } from '@angular/core';

import { Comment, Entry } from './../../../models/index';
import { CommentService, EntryService } from './../../../services/index';

@Component({
  selector: 'ch-comment-short',
  templateUrl: './comment-short.component.html',
  styleUrls: ['./comment-short.component.scss']
})
export class CommentShortComponent implements OnInit {

  @Input()title: string;
  @Input()items: Comment[];

  constructor(private commentService: CommentService, private entryService: EntryService) { }

  ngOnInit() {
  }

  getItem(entry: Entry) {
    this.entryService.selectEntry.emit(entry);
  }

}
