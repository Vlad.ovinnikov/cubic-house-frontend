import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { AuthService } from './../../../../services';

@Component({
  selector: 'ch-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output()removeUser: EventEmitter<string>;
  menuItems: any[] = [];
  actionsMenu: any[] = [];

  constructor(private authService: AuthService) {
    this.removeUser = new EventEmitter<string>();
  }

  ngOnInit() {
    this.menuItems = [
      {
        name: 'MENU.HOME',
        url: '/news',
        isUser: true
      },
      {
        name: 'MENU.POSTS',
        url: '/user/posts',
        isUser: true
      },
      {
        name: 'MENU.COMMENTS',
        url: '/user/comments',
        isUser: true
      },
      {
        name: 'MENU.MESSAGES',
        url: '/user/messages',
        isUser: true
      },
      {
        name: 'MENU.CATEGORIES',
        url: '/user/categories',
        isUser: false
      }
    ];

    this.actionsMenu = [
      {
        name: 'languageSetName',

      }
    ];
  }

  isAdmin(isUser: boolean): boolean {
    return this.authService.isAdmin(isUser);
  }

  logout(event) {
    event.preventDefault();

    this.removeUser.emit('remove');
    this.authService.logout()
      .subscribe(
        data => console.log(data),
        err => console.error(err)
      );
  }
}
