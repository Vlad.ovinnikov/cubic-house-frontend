import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Entry } from './../../../models';
import { EntryService } from './../../../services';

@Component({
  selector: 'ch-post-short',
  templateUrl: './post-short.component.html',
  styleUrls: ['./post-short.component.scss']
})
export class PostShortComponent implements OnInit {

  @Input()title: string;
  @Input()items: Entry[];

  constructor(private entryService: EntryService) { }

  ngOnInit() {
  }

  getItem(entry: Entry) {
    this.entryService.selectEntry.emit(entry);
  }
}
