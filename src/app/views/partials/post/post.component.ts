import { Component, OnInit, Input } from '@angular/core';

import { Entry } from './../../../models';

@Component({
  selector: 'ch-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input()entry: Entry;
  @Input()index: number;

  constructor() { }

  ngOnInit() {
  }

}
