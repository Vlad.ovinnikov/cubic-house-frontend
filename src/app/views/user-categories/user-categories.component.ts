import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { CategoryService } from './../../services';
import { TranslateService } from '@ngx-translate/core';
import { Category } from './../../models';

@Component({
  selector: 'ch-user-categories',
  templateUrl: './user-categories.component.html',
  styleUrls: ['./user-categories.component.scss']
})
export class UserCategoriesComponent implements OnInit {

  isRequesting: boolean;
  categories: Category[];
  currentCategory: Category;
  showAdd: boolean;
  isEdit: boolean;
  message: any;

  categoryForm: FormGroup;
  title: FormControl;
  description: FormControl;

  constructor(private categoryService: CategoryService, private translate: TranslateService) { }

  ngOnInit() {
    this.showAdd = false;
    this.currentCategory = new Category();
    this.isRequesting = true;
    this.getCategories();
    this.createControls();
    this.createForm();
    // this.currentCategory = this.categories[0];
  }

  createControls(): void {
    this.title = new FormControl(null, Validators.compose([Validators.required, Validators.minLength(3)]));
    this.description = new FormControl(null);
  }

  createForm(): void {
    this.categoryForm = new FormGroup({
      'title': this.title,
      'description': this.description
    });
  }

  save(): void {
    this.isRequesting = true;
    if (this.isEdit) {
      this.categoryService.update(this.currentCategory._id, this.categoryForm.value)
        .subscribe(
          (data: Category) => {
            this.showAdd = !this.showAdd;
            this.currentCategory = data;
            this.isRequesting = false;
            this.message = { message: this.translate.instant('CATEGORIES.SUCCESS_UPDATE'), error: 'success' };
            setTimeout(() => {
              this.message = null;
            }, 5000);
          },
          (err: any) => {
            this.isRequesting = false;
            this.message = { message: err.message, error: 'danger' };
          }
        );
    } else {
      this.categoryService.add(this.categoryForm.value)
        .subscribe(
          (data: Category) => {
            console.log(data);
            this.showAdd = !this.showAdd;
            this.currentCategory = data;
            this.message = { message: this.translate.instant('CATEGORIES.SUCCESS_ADD'), error: 'success' };
            this.categoryForm.reset();
            this.getCategories();
            setTimeout(() => {
              this.message = null;
            }, 5000);
          },
          (err: any) => {
            this.isRequesting = false;
            this.message = { message: err.message, error: 'danger' };
          }
        );
    }
  }

  remove(category: Category) {
    this.categoryService.remove(category._id)
      .subscribe(
        (data: any) => {
          console.log(data);
          this.isRequesting = false;
          this.message = { message: data.message, error: 'success' };
          this.getCategories();
          setTimeout(() => {
            this.message = null;
          }, 5000);
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: 'danger' };
        }
      );
  }

  getCategory(category: Category): void {
    this.isRequesting = true;
    this.categoryService.get(category._id)
      .subscribe(
        (data: Category) => {
          this.isRequesting = false;
          this.currentCategory = data;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: 'danger' };
        }
      );
  }

  cancelAdd() {
    this.showAdd = !this.showAdd;
  }

  openAddCategory() {
    this.isEdit = false;
    this.showAdd = !this.showAdd;
  }

  openEditCategory(category: Category) {
    this.title.setValue(category.title);
    this.description.setValue(category.description);
    this.isEdit = true;
    this.showAdd = !this.showAdd;
  }

  private getCategories(): void {
    this.categoryService.getList()
      .subscribe(
        (data: Category[]) => {
          // this.currentCategory = data[0];
          this.categories = data;
          this.isRequesting = false;
        },
        (err: any) => {
          this.isRequesting = false;
          this.message = { message: err.message, error: true };
        }
      );
  }
}
