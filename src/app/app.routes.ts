import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import { HOME_ROUTES } from './views/home/home.routes';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { ENTER_ROUTES, EnterComponent } from './views/enter';
import { AuthGuardService } from './services';
import { UserPostsComponent } from './views/user-posts/user-posts.component';
import { UserMessagesComponent } from './views/user-messages/user-messages.component';
import { UserCommentsComponent } from './views/user-comments/user-comments.component';
import { UserCategoriesComponent } from './views/user-categories/user-categories.component';
import { UserProfileComponent } from './views/user-profile/user-profile.component';
import { UserSettingsComponent } from './views/user-settings/user-settings.component';

const APP_ROUTES: Routes = [

  { path: '', component: HomeComponent, canActivate: [AuthGuardService], children: HOME_ROUTES},
  { path: 'enter', component: EnterComponent, children: ENTER_ROUTES },
  { path: 'user/posts', component: UserPostsComponent, canActivate: [AuthGuardService] },
  { path: 'user/messages', component: UserMessagesComponent, canActivate: [AuthGuardService] },
  { path: 'user/comments' , component: UserCommentsComponent, canActivate: [AuthGuardService] },
  { path: 'user/categories' , component: UserCategoriesComponent, canActivate: [AuthGuardService] },
  { path: 'user/profile', component: UserProfileComponent, canActivate: [AuthGuardService] },
  { path: 'user/settings', component: UserSettingsComponent, canActivate: [AuthGuardService] },

  { path: '**', redirectTo: '404' },
  { path: '404', component: NotFoundComponent }

];

export const RoutesModule = RouterModule.forRoot(APP_ROUTES);
