import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';

import { EntryService, HttpClientService } from './../.';

class RouterStub {
  navigateByUrl = jasmine.createSpy('navigateByUrl');
}

describe('EntryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        EntryService,
        HttpClientService,
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  it('should ...', inject([EntryService], (service: EntryService) => {
    expect(service).toBeTruthy();
  }));
});
