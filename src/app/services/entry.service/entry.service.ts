import { Injectable, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';

import { HttpClientService} from './..';
import { ErrorHandler } from './../errorHandler';
import { Entry } from './../../models';
import { API_URL } from './../../app.settings';

@Injectable()
export class EntryService extends ErrorHandler {

  selectEntry: EventEmitter<Entry>;
  entryEvent: EventEmitter<any>;

  constructor(private http: HttpClientService) {
    super();
    this.selectEntry = new EventEmitter<Entry>();
    this.entryEvent = new EventEmitter<any>();
  }

  getListWithCategory(categoryId: string, page: Number, perPage: Number) {
    return this.http.get(`${ API_URL }/categories/${ categoryId }/entries/?page=${ page }&perPage=${ perPage }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.getListWithCategory: ${ new Date() }`);
          });
  }

  getList(page: Number, perPage: Number) {
    return this.http.get(`${ API_URL }/entries/?page=${ page }&perPage=${ perPage }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.getList: ${ new Date() }`);
          });
  }

  getUserList(page: Number, perPage: Number) {
    return this.http.get(`${ API_URL }/user/entries/?page=${ page }&perPage=${ perPage }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.getUserList: ${ new Date() }`);
          });
  }

  getRecent() {
    return this.http.get(`${ API_URL }/entries/recent`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.getRecent: ${ new Date() }`);
          });
  }

  getUserListWithCategory(categoryId: string, page: Number, perPage: Number) {
    return this.http.get(`${ API_URL }/user/entries/?category=${categoryId}&page=${ page }&perPage=${ perPage }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.getUserList: ${ new Date() }`);
          });
  }

  get(id: string) {
    return this.http.get(`${ API_URL }/entries/${ id }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.get: ${ new Date() }`);
          });
  }

  add(entry: Entry) {
    return this.http.post(`${ API_URL }/entries`, entry)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.add: ${ new Date() }`);
          });
  }

  update(entry: Entry) {
    return this.http.put(`${ API_URL }/entries/${ entry._id }`, entry)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.update: ${ new Date() }`);
          });
  }

  remove(id: string) {
    return this.http.delete(`${ API_URL }/entries/${ id }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`Entry.remove: ${ new Date() }`);
          });
  }

}
