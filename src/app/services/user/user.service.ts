import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { ErrorHandler } from './../errorHandler';
import { HttpClientService} from './..';
import { API_URL } from './../../app.settings';
import { Profile } from './../../models';

@Injectable()
export class UserService extends ErrorHandler {

  constructor(private http: HttpClientService) {
    super();
  }

  getProfile() {
    return this.http.get(`${ API_URL }/user/profile`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`User.getProfile: ${ new Date() }`);
          });
  }

  updateProfile(profile: Profile) {
    return this.http.put(`${ API_URL }/user/profile`, profile)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`User.updateProfile: ${ new Date() }`);
          });
  }

  updateEmail(email: string) {
    return this.http.put(`${ API_URL }/user/email`, { email: email })
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`User.updateEmail: ${ new Date() }`);
          });
  }

  updatePassword(password: string) {
    return this.http.put(`${ API_URL }/user/password`, { password: password })
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`User.updatePassword: ${ new Date() }`);
          });
  }
}
