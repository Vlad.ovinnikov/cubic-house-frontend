import { Injectable, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';

import { HttpClientService } from './..';
import { ErrorHandler } from './../errorHandler';
import { API_URL } from './../../app.settings';
import { Comment } from './../../models';

@Injectable()
export class CommentService extends ErrorHandler {

  commentEvent: EventEmitter<any>;

  constructor(private http: HttpClientService) {
    super();
    this.commentEvent = new EventEmitter<any>();
  }

  getList(id: string) {
    return this.http.get(`${ API_URL }/entries/${ id }/comments`)
          .map(
            (res: Response) =>  res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CommentService.getList: ${ new Date() }`);
          });
  }

  getRecent() {
    return this.http.get(`${ API_URL }/comments/recent`)
          .map(
            (res: Response) =>  res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CommentService.getRecent: ${ new Date() }`);
          });
  }

  add(comment: Comment) {
    return this.http.post(`${ API_URL }/comments`, comment)
          .map(
            (res: Response) =>  res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CommentService.add: ${ new Date() }`);
          });
  }

  update(comment: Comment) {
    return this.http.put(`${ API_URL }/comments/${ comment._id }`, comment)
          .map(
            (res: Response) =>  res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CommentService.update: ${ new Date() }`);
          });
  }

  remove(entryId: string, commentId: string) {
    return this.http.delete(`${ API_URL }/entries/${ entryId }/comments/${ commentId }`)
          .map(
            (res: Response) =>  res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CommentService.remove: ${ new Date() }`);
          });
  }
}
