import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';

import { CommentService, HttpClientService } from './../.';

class RouterStub {
  navigateByUrl = jasmine.createSpy('navigateByUrl');
}

describe('CommentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        CommentService,
        HttpClientService,
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  it('should ...', inject([CommentService], (service: CommentService) => {
    expect(service).toBeTruthy();
  }));
});
