import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';

import { AuthService, HttpClientService, LocalStorageService } from './../.';

class RouterStub {
  navigateByUrl = jasmine.createSpy('navigateByUrl');
}

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        AuthService,
        HttpClientService,
        LocalStorageService,
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  it('should works', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));
});
