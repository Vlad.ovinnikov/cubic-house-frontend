import { Injectable, EventEmitter, Output } from '@angular/core';
import { Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

import { User } from './../../models';
import { API_URL } from './../../app.settings';
import { LocalStorageService } from './../local-storage.service/local-storage.service';
import { HttpClientService } from './../http-client.service/http-client.service';
import { ErrorHandler } from './../errorHandler';

@Injectable()
export class AuthService extends ErrorHandler {

  private subject: Subject<string>;
  statusAuth: EventEmitter<string>;
  authEvent: EventEmitter<any>;

  constructor(private http: HttpClientService, private router: Router, private localStorageService: LocalStorageService) {
    super();
    this.subject = new Subject<string>();
    this.statusAuth = new EventEmitter<string>();
    this.authEvent = new EventEmitter<any>();
  }

  signup(user: User) {
    return this.http.post(`${ API_URL }/signup`, user)
                .map(
                  (res: Response) => { return res.json(); }
                )
                .catch(this.errorHandler)
                .finally(() => {
                  console.info(`AuthService.signup: ${ new Date() }`);
                });
  }

  login(user: User) {
    return this.http.post(`${ API_URL }/login`, user)
                .map(
                  (res: Response) => {
                    // login successful if there's a jwt token in the response
                    const userJson = res.json();

                     if (userJson && userJson.token) {
                      // store user details and jwt token in local storage to keep user logged in between page refreshes
                      this.localStorageService.set('ch.jwt_token', userJson.token);
                      this.localStorageService.set('ch.user', userJson.user);
                      this.localStorageService.set('ch.profile', userJson.profile);

                      this.subject.next(userJson.token);
                      this.router.navigate(['/']);

                      return { user: userJson.user, profile: userJson.profile };
                    }
                  }
                )
                .catch(this.errorHandler)
                .finally( () => {
                  console.info(`AuthService.login: ${ new Date() }`);
                });
  }

  confirmEmail(token: string) {
    return this.http.post(`${ API_URL }/email-confirm`, { token })
            .map((res: Response) => { return res.json(); })
            .catch(this.errorHandler)
            .finally( () => {
              console.info(`AuthService.confirmEmail: ${ new Date() }`);
            });
  }

  forgotPassword(email: string) {
    return this.http.post(`${ API_URL }/forgot`, email)
            .map((res: Response) => { return res.json(); })
            // .catch(this.errorHandler)
            .finally( () => {
              console.info(`AuthService.forgotPassword: ${ new Date() }`);
            });
  }

  resetPassword(hash: string, password: string) {
    return this.http.post(`${ API_URL }/reset/${ hash }`, { password })
            .map((res: Response) => {return res.json(); })
            .catch(this.errorHandler)
            .finally( () => {
              console.info(`AuthService.resetPassword: ${ new Date() }`);
            });
  }

  getLogged(): Observable<string> {
    return this.subject.asObservable();
  }

  logout() {
    return this.http.delete(`${ API_URL }/logout`)
            .map((res: Response) => {
              // remove token and user from local storage to log user out
              localStorage.removeItem('ch.jwt_token');
              localStorage.removeItem('ch.user');
              localStorage.removeItem('ch.profile');

              this.router.navigate(['enter/login']);
              return res;
            })
            .catch(this.errorHandler)
            .finally( () => {
              console.info(`AuthService.logout: ${ new Date() }`);
            });
  }

  isAuthenticated(): boolean {
     if (this.localStorageService.get('ch.jwt_token')) {
      return true;
    } else {
      localStorage.removeItem('ch.jwt_token');
      localStorage.removeItem('ch.user');
      localStorage.removeItem('ch.profile');
      this.router.navigate(['enter/login']);
      return false;
    }
  }

  isAdmin(isUser: boolean): boolean {
     if (!isUser) {
       if (this.localStorageService.getObject('ch.user').role === 'USER') {
        return false;
      }
      return true;
    } else {
      return true;
    }
  }

}
