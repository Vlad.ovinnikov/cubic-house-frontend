import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  private storage = this.isStorageSupported('localStorage') ? localStorage : localStorage;

  constructor() { }

  /**
   *  Set localStorage item or object
   * @param  {[type]} key
   * @param  {[type]} value
   * @return {[type]}
   */
  set(key, value) {
    if (typeof(value) === 'object') {
      this.storage.setItem(key, JSON.stringify(value));
    } else {
      this.storage.setItem(key, value);
    }
  }

  get(key): any {
    return this.storage.getItem(key);
  }

  getObject(key): any {
    return JSON.parse(this.storage.getItem(key));
  }

  removeItem(key) {
    this.storage.removeItem(key);
  }

  private isStorageSupported(storageName) {
    const testKey = 'test';
    const storage: any = window[storageName];
    try {
      storage.setItem(testKey, '1');
      storage.removeItem(testKey);
      return true;
    }
    catch (error) {
      return false;
    }
  }
}
