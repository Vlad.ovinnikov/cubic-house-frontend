import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';

@Injectable()
export class ErrorHandler {

  constructor() {}

  errorHandler(error: any): any {
    return Observable.throw(error.json());
  }
}
