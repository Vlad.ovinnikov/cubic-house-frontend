import { Injectable, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';

import { HttpClientService } from './..';
import { ErrorHandler } from './../errorHandler';
import { API_URL } from './../../app.settings';
import { Category } from './../../models';

@Injectable()
export class CategoryService extends ErrorHandler {

  categoriesSharing: EventEmitter<Category[]>;
  selectCategory: EventEmitter<Category>;

  constructor(private http: HttpClientService) {
    super();
    this.categoriesSharing = new EventEmitter<Category[]>();
    this.selectCategory = new EventEmitter<Category>();
  }

  getList() {
    return this.http.get(`${ API_URL }/categories`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CategoryService.getList: ${ new Date() }`);
          });
  }

  get(id: string) {
    return this.http.get(`${ API_URL }/categories/${ id }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CategoryService.get: ${ new Date() }`);
          });
  }

  add(category: Category) {
    return this.http.post(`${ API_URL }/categories`, category)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CategoryService.add: ${ new Date() }`);
          });
  }

  update(id: string, category: Category) {
    return this.http.put(`${ API_URL }/categories/${ id }`, category)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
            console.info(`CategoryService.update: ${ new Date() }`);
          });
  }

  remove(id: string) {
    return this.http.delete(`${ API_URL }/categories/${ id }`)
          .map(
            (res: Response) => res.json()
          )
          .catch(this.errorHandler)
          .finally(() => {
             console.info(`CategoryService.remove: ${ new Date() }`);
          });
  }
}
