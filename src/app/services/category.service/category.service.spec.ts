import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';

import { CategoryService, HttpClientService } from './../.';

class RouterStub {
  navigateByUrl = jasmine.createSpy('navigateByUrl');
}

describe('CategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        CategoryService,
        HttpClientService,
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  it('should ...', inject([CategoryService], (service: CategoryService) => {
    expect(service).toBeTruthy();
  }));
});
