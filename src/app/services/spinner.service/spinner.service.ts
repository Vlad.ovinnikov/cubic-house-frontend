import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SpinnerService {

  runSpinner: EventEmitter<boolean>;

  constructor() {
  this.runSpinner = new EventEmitter<boolean>();
 }

}
