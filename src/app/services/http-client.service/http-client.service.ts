import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

import { LANG_HEADER, AUTH_HEADER } from './../../app.settings';

@Injectable()
export class HttpClientService {

  constructor(private http: Http, private router: Router) { }

  // GET
  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
   return this.intercept(this.http.get(url, this.getRequestOptionArgs(options)));
 }
  // POST
  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(this.http.post(url, body, this.getRequestOptionArgs(options)));
  }
  // PUT
  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(this.http.put(url, body, this.getRequestOptionArgs(options)));
  }
  // DELETE
  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(this.http.delete(url, this.getRequestOptionArgs(options)));
 }

  private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    const token = localStorage.getItem('ch.jwt_token');
    const lang = localStorage.getItem('ch.lang') || 'en';

     if (options == null) {
      options = new RequestOptions();
    }
     if (options.headers == null) {
      options.headers = new Headers();
    }
    options.headers.append(LANG_HEADER, lang);

     if (token) {
      options.headers.append(AUTH_HEADER, token);
    }

    options.headers.append('Content-Type', 'application/json');
    return options;
  }

  private intercept(observable: Observable<Response>): Observable<Response> {
    return observable.catch((err, source) => {
       if (err.status  === 401 ) {// && !_.endsWith(err.url, 'api/auth/login')) {
        localStorage.removeItem('ch.jwt_token');
        this.router.navigate(['enter/login']);
        return Observable.empty();
      } else {
          return Observable.throw(err);
      }
    });
  }

}
