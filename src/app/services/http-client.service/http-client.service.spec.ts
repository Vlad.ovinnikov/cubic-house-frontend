import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';

import { HttpClientService } from './../.';

class RouterStub {
  navigateByUrl = jasmine.createSpy('navigateByUrl');
}

describe('HttpClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        HttpClientService,
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  it('should ...', inject([HttpClientService], (service: HttpClientService) => {
    expect(service).toBeTruthy();
  }));
});
