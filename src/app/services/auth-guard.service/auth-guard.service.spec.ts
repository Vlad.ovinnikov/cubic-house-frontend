import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';

import { AuthGuardService, AuthService, LocalStorageService, HttpClientService } from './../.';

class RouterStub {
  navigateByUrl = jasmine.createSpy('navigateByUrl');
}

describe('AuthGuardService', () => {
  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        AuthGuardService,
        AuthService,
        HttpClientService,
        LocalStorageService,
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  it('should works', inject([AuthGuardService], (service: AuthGuardService) => {
    expect(service).toBeTruthy();
  }));
});
