import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

import { AuthService , LocalStorageService} from './services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ch-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './../assets/styles/base.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit, OnDestroy {

  // public translatedText: string;
  // public langs: any[];
  public currentUser: any;
  // private currentLang: string;
  private subGetLogged: any;
  private subAuthStatus: any;

  constructor(private authService: AuthService, private translate: TranslateService, private localStorageService: LocalStorageService) {
    translate.addLangs(['en', 'pl']);
    translate.setDefaultLang('en');

    let browserLang = translate.getBrowserLang();
    let currentLang = browserLang.match(/en|pl/) ? browserLang : 'en';
    translate.use(currentLang);
    this.localStorageService.set('ch.lang', currentLang);
  }

  ngOnInit() {

    // this.langs = [
    //     { display: 'English', value: 'en' },
    //     { display: 'Polish', value: 'pl' }
    // ];

    // this.currentLang = this.localStorageService.get('ch.lang');
    //
    // if (!this.currentLang) {
    //   this.currentLang = navigator.language.substring(0, 2);
    //   this.localStorageService.set('ch.lang', this.currentLang);
    // }

    // set current langage
    // this.selectLang(this.currentLang);

    this.subGetLogged = this.authService.getLogged()
      .subscribe((logged: string) => {
        this.currentUser = logged;
    });

    this.currentUser = this.localStorageService.get('ch.jwt_token');

    this.subAuthStatus = this.authService.statusAuth
      .subscribe((status: string) => {
        if (status === 'logout') {
          this.currentUser = null;
        } else if (status === 'login') {
          this.currentUser = this.localStorageService.get('ch.jwt_token');
        }
      });
  }

  ngOnDestroy() {
    this.subGetLogged.unsubscribe();
    this.subAuthStatus.unsubscribe();
  }

  // isCurrentLang(lang: string) {
  //   // check if the selected lang is current lang
  //   return lang === this.translate.currentLang;
  // }

  // selectLang(lang: string) {
  //   // set current lang;
  //   this.translate.use(lang);
  //   this.refreshText();
  // }

  // refreshText() {
  //   // refresh translation when language change
  //   this.translatedText = this.translate.instant('passwordPlaceholder');
  // }

  removeUserCookie(removeUser: string) {
    this.currentUser = null;
  }
}
