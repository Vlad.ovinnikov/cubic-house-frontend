import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ch-message-alert',
  templateUrl: './message-alert.component.html',
  styleUrls: ['./message-alert.component.scss']
})
export class MessageAlertComponent implements OnInit {

  @Input()message: any;

  constructor() { }

  ngOnInit() {
  }

}
