import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { Http } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NavMenuComponent, SpinnerComponent, MessageAlertComponent } from './../.';
import { RoutesModule } from './../../app.routes';
import { UserCategoriesComponent } from './../../views';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpTranslationsLoader } from './../../../assets/translations/httpTranslationsLoader';

describe('NavMenuComponent', () => {
  let component: NavMenuComponent;
  let fixture: ComponentFixture<NavMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavMenuComponent, UserCategoriesComponent, SpinnerComponent, MessageAlertComponent ],
      imports:[
        RouterModule.forRoot([{ path: 'user/categories' , component: UserCategoriesComponent }]),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpTranslationsLoader.HttpLoaderFactory,
            deps: [Http]
          }
        }),
        ReactiveFormsModule, FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
