import { Component, OnInit, Input } from '@angular/core';

import { Category } from './../../models';
import { CategoryService } from './../../services';

@Component({
  selector: 'ch-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {

  @Input() items: Category[];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
  }

  getItem(category: Category) {
    this.categoryService.selectCategory.emit(category);
  }

}
