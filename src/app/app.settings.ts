import { environment } from './../environments/environment';

export const API_URL = environment.API_URL;
export const AUTH_HEADER = 'x-auth';
export const LANG_HEADER = 'x-lang';
