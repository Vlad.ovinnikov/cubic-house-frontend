import { TestBed, async } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Http, HttpModule}  from '@angular/http';
import { AppComponent } from './app.component';
import { RoutesModule } from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  HomeComponent, NewsComponent, PostsComponent, PostDetailsComponent, AddPostComponent, EditPostComponent, EnterComponent, ConfirmEmailComponent, LoginComponent, SignupComponent, ForgotPasswordComponent, ResetPasswordComponent, UserPostsComponent, UserMessagesComponent, UserCommentsComponent, UserCategoriesComponent, ProfileComponent, PasswordComponent, EmailComponent, UserProfileComponent, UserSettingsComponent, NotFoundComponent, NavbarComponent, FooterComponent, PaginationComponent, PostShortComponent, AddEditPostComponent, PostComponent, CommentItemComponent, ReplyComponent, CommentComponent, CommentShortComponent
} from './views';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpTranslationsLoader } from './../assets/translations/httpTranslationsLoader';

import { AuthService, AuthGuardService, LocalStorageService, HttpClientService, CategoryService, EntryService, CommentService,  SpinnerService,  UserService } from './services';

import { SpinnerComponent, NavMenuComponent, MessageAlertComponent } from './components/';
import { TruncatePipe } from './pipes/';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        RoutesModule,
        ReactiveFormsModule,
        FormsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpTranslationsLoader.HttpLoaderFactory,
            deps: [Http]
          }
        })
      ],
      declarations: [
        AppComponent, NavbarComponent, FooterComponent, HomeComponent, NewsComponent, PostsComponent, PostDetailsComponent, AddPostComponent, EditPostComponent, EnterComponent, ConfirmEmailComponent, LoginComponent, SignupComponent, ForgotPasswordComponent, ResetPasswordComponent,UserPostsComponent, UserMessagesComponent, UserCommentsComponent, UserCategoriesComponent, ProfileComponent, PasswordComponent, EmailComponent, UserProfileComponent, UserSettingsComponent, NotFoundComponent, SpinnerComponent, NavMenuComponent, MessageAlertComponent, PaginationComponent, PostShortComponent, AddEditPostComponent, PostComponent, CommentItemComponent, ReplyComponent, CommentShortComponent, CommentComponent, TruncatePipe
      ],
      providers: [
        AuthService, AuthGuardService, LocalStorageService, HttpClientService, CategoryService, EntryService, CommentService,  SpinnerService,  UserService, { provide: APP_BASE_HREF, useValue: '/' }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  // it(`should have as title 'ch works!'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('ch works!');
  // }));

  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('ch works!');
  // }));
});
