import { Serializable } from './../helpers/serializable';
import { Profile } from './index';

export class User extends Serializable {
  email: string;
  password: string;
  passwordUpdatedAt: string;
  createdAt: Date;
  updatedAt: Date;
  role: string;
  profile: Profile;

  constructor() {
    super();
  }
}
