import { Serializable } from './../helpers/serializable';
import { Comment, Category, Profile } from './index';

export class Entry extends Serializable {

  _id: string;
  title: string;
  description: string;
  priority: Number;
  status: Number;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  author: Profile;
  category: Category;
  comments: Comment[];

  constructor() {
    super();
  }

}
