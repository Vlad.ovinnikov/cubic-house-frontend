export class Profile {
  firstName: string;
  lastName: string;
  avatar: string;
  gender: boolean;
  birthdate: Date;
  createdAt: Date;
  updatedAt: Date;
  _userId: string;

  // constructor(
  //   fisrtName: string,
  //   lastName: string,
  //   avatar: string,
  //   gender: boolean,
  //   birthdate: Date
  // ) {
  //   this.firstName = fisrtName;
  //   this.lastName = lastName;
  //   this.avatar = avatar;
  //   this.gender = gender;
  //   this.birthdate = birthdate;
  // }
}
