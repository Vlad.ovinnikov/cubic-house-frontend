import { Profile } from './../models';

export class Category {
  _id: string;
  title: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  author: Profile;

}
