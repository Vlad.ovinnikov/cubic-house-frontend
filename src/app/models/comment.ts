import { Profile } from './index';

export class Comment {

  _id: string;
  text: string;
  parentId: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  author: Profile;
  entry: string;
  votes: Object[];
  children: Object[];

}
