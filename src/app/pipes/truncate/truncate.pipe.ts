import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const limit = parseInt(args, 10);

    return value.length > limit ? value.substring(0, limit) + '...' : value;
  }

}
