import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
// app
import { AppComponent } from './app.component';
import { RoutesModule } from './app.routes';
// views
import {
  NotFoundComponent,
  EnterComponent,
  LoginComponent,
  SignupComponent,
  ForgotPasswordComponent,
  ResetPasswordComponent,
  ConfirmEmailComponent,
  HomeComponent,
  HOME_ROUTES,
  NewsComponent,
  PostsComponent,
  PostDetailsComponent,
  AddPostComponent,
  EditPostComponent,
  UserPostsComponent,
  UserCommentsComponent,
  UserMessagesComponent,
  UserCategoriesComponent,
  ProfileComponent,
  PasswordComponent,
  EmailComponent,
  UserProfileComponent,
  UserSettingsComponent,
  NavbarComponent,
  FooterComponent,
  PaginationComponent,
  PostShortComponent,
  AddEditPostComponent,
  PostComponent,
  CommentItemComponent,
  ReplyComponent,
  CommentComponent,
  CommentShortComponent
} from './views';

// services
import {
  AuthService,
  AuthGuardService,
  LocalStorageService,
  HttpClientService,
  CategoryService,
  EntryService,
  CommentService,
  SpinnerService,
  UserService
} from './services';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpTranslationsLoader } from './../assets/translations/httpTranslationsLoader';

// import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// pipes
import { TruncatePipe } from './pipes/';

// directives
import { AutosizeTextareaDirective } from './directives/autosize-textarea.directive';

// components
import { SpinnerComponent, NavMenuComponent, MessageAlertComponent } from './components/';

// export function HttpLoaderFactory(http: Http) {
//     return new TranslateHttpLoader(http, './assets/translations/i18n-', '.json');
// }

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    EnterComponent,
    FooterComponent,
    NotFoundComponent,
    LoginComponent,
    SignupComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ConfirmEmailComponent,
    TruncatePipe,
    PaginationComponent,
    PostComponent,
    PostsComponent,
    NavMenuComponent,
    PostShortComponent,
    PostDetailsComponent,
    NewsComponent,
    AddPostComponent,
    UserPostsComponent,
    UserCommentsComponent,
    UserMessagesComponent,
    AddEditPostComponent,
    EditPostComponent,
    AutosizeTextareaDirective,
    MessageAlertComponent,
    CommentComponent,
    ReplyComponent,
    CommentItemComponent,
    CommentShortComponent,
    UserCategoriesComponent,
    SpinnerComponent,
    UserProfileComponent,
    UserSettingsComponent,
    ProfileComponent,
    EmailComponent,
    PasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    Ng2PageScrollModule.forRoot(),
    RoutesModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpTranslationsLoader.HttpLoaderFactory,
        deps: [Http]
      }
    })
  ],
  providers: [
    AuthService,
    AuthGuardService,
    LocalStorageService,
    HttpClientService,
    CategoryService,
    EntryService,
    CommentService,
    SpinnerService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
